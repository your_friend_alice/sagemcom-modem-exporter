#!/usr/bin/env python3

import asyncio
import os
from sagemcom_api.enums import EncryptionMethod
from sagemcom_api.client import SagemcomClient
from http.server import BaseHTTPRequestHandler, HTTPServer
import json

USERNAME = os.getenv("MODEM_USERNAME", "guest")
PASSWORD = os.getenv("MODEM_PASWORD", "")
ENCRYPTION_METHOD = {
        "MD5": EncryptionMethod.MD5,
        "SHA512": EncryptionMethod.SHA512
        }[os.getenv("MODEM_ENCRYPTION_METHOD", "MD5")]
LISTEN_ADDRESS = os.getenv("LISTEN_ADDRESS", "0.0.0.0")
LISTEN_PORT = int(os.getenv("LISTEN_PORT", "9090"))

class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            host = self.path.strip("/")
            result = status(host)
        except Exception as e:
            self.send_response(500)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write("Internal Server Error\n".encode())
            raise e

        self.send_response(200)
        self.send_header("Content-type", "text/plain; version=0.0.4")
        self.end_headers()
        value = int(result is not None)
        self.wfile.write((
            "# HELP modem_up Whether the modem is connected to the internet or not.\n# TYPE modem_up gauge\nmodem_up{address="+
            json.dumps(host)+
            ", public_address=" + json.dumps(result or "") +
            "} " + str(value) + "\n"
            ).encode())


def status(host) -> str:
    async def inner() -> str:
        async with SagemcomClient(host, USERNAME, PASSWORD, ENCRYPTION_METHOD) as client:
            await client.login()
            # Print device information of Sagemcom F@st router
            device_info = await client.get_device_info()
            if await client.get_value_by_xpath("Device/DSL/Lines/Line[@uid='1']/LinkStatus") == "UP":
                return await client.get_value_by_xpath("Device/IP/Interfaces/Interface[@uid='2']/IPv4Addresses/IPv4Address[@uid='1']/IPAddress")
            else:
                return None
    return asyncio.run(inner())

if __name__ == "__main__":
    HTTPServer((LISTEN_ADDRESS, LISTEN_PORT), Server).serve_forever()
