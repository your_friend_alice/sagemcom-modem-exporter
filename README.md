# sagemcom-modem-exporter

Export a single metric: "modem_up", for the state of your Sagemcom modem, labeled with its public IP address.

## Usage

Point your prometheus at:

```
http://<this_exporter>:9090/<my_modem_address>
```

## Environment Variables

| Name | Description | Default |
|------|-------------|---------|
| `LISTEN_ADDRESS` | Address for the exporter to listen on. | "0.0.0.0" |
| `LISTEN_PORT` | Port for the exporter to listen on. | 9090 |
| `MODEM_USERNAME` | Username to log in to the modem. You do not need admin. | "guest" |
| `MODEM_PASSWORD` | Password to log in to the modem. You do not need admin. | "" |
| `MODEM_ENCRYPTION_METHOD` | Encryption method to log in to the modem. Either "MD5" or "SHA512". | "MD5" |

See https://pypi.org/project/sagemcom-api/ for values to use for the `MODEM_` variables.

## Building

```
docker build -t sagemcom-modem-exporter .
```
