FROM python:3.10-alpine
RUN mkdir /exporter
WORKDIR /exporter
COPY requirements.txt /exporter/
RUN pip install -r requirements.txt
COPY exporter.py /exporter/
CMD /exporter/exporter.py
USER 1000
